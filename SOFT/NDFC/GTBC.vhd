--GTBC--Gray code to binary converter--
--https://vhdlguru.blogspot.com/2020/12/generic-vhdl-code-for-binary-to-gray.html
Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;
entity GTBC is
	generic(N: integer :=32);--Number of bits of the counter
	port(	GRAY : IN std_logic_vector (N-1 DOWNTO 0);--Input data
			BINARY : OUT std_logic_vector (N-1 DOWNTO 0)--Latched data
	    );
end GTBC;
--

architecture GRAY_TO_BIN_ARCH of GTBC is
signal TEMP : std_logic_vector(N-1 downto 0);

begin
TEMP(N-1) <= GRAY(N-1);
--XOR GENERATOR.
XORGATES : for i in N-2 downto 0 generate
    TEMP(i) <= TEMP(i+1) xor GRAY(i);
end generate;    

BINARY <= TEMP;
end GRAY_TO_BIN_ARCH;