-- Copyright (C) 2022  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "09/02/2022 11:26:10"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          NDFC
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY NDFC_vhd_vec_tst IS
END NDFC_vhd_vec_tst;
ARCHITECTURE NDFC_arch OF NDFC_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL FREQ : STD_LOGIC;
SIGNAL FRQCOUNTBIN : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL FRQCOUNTLATCH : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL LATCH : STD_LOGIC;
SIGNAL REF_CLK : STD_LOGIC;
SIGNAL REG_ACCES : STD_LOGIC;
SIGNAL RST : STD_LOGIC;
COMPONENT NDFC
	PORT (
	FREQ : IN STD_LOGIC;
	FRQCOUNTBIN : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	FRQCOUNTLATCH : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	LATCH : OUT STD_LOGIC;
	REF_CLK : IN STD_LOGIC;
	REG_ACCES : IN STD_LOGIC;
	RST : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : NDFC
	PORT MAP (
-- list connections between master ports and signals
	FREQ => FREQ,
	FRQCOUNTBIN => FRQCOUNTBIN,
	FRQCOUNTLATCH => FRQCOUNTLATCH,
	LATCH => LATCH,
	REF_CLK => REF_CLK,
	REG_ACCES => REG_ACCES,
	RST => RST
	);

-- REF_CLK
t_prcs_REF_CLK: PROCESS
BEGIN
LOOP
	REF_CLK <= '0';
	WAIT FOR 100000 ps;
	REF_CLK <= '1';
	WAIT FOR 100000 ps;
	IF (NOW >= 2000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_REF_CLK;

-- FREQ
t_prcs_FREQ: PROCESS
BEGIN
LOOP
	FREQ <= '0';
	WAIT FOR 10000 ps;
	FREQ <= '1';
	WAIT FOR 10000 ps;
	IF (NOW >= 2000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_FREQ;

-- REG_ACCES
t_prcs_REG_ACCES: PROCESS
BEGIN
	FOR i IN 1 TO 4
	LOOP
		REG_ACCES <= '0';
		WAIT FOR 406800 ps;
		REG_ACCES <= '1';
		WAIT FOR 45200 ps;
	END LOOP;
	REG_ACCES <= '0';
WAIT;
END PROCESS t_prcs_REG_ACCES;
END NDFC_arch;
