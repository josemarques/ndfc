-- Copyright (C) 2022  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 21.1.1 Build 850 06/23/2022 SJ Lite Edition"

-- DATE "09/02/2022 13:08:32"

-- 
-- Device: Altera EPM240T100C5 Package TQFP100
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY MAXII;
USE IEEE.STD_LOGIC_1164.ALL;
USE MAXII.MAXII_COMPONENTS.ALL;

ENTITY 	NDFC IS
    PORT (
	RST : OUT std_logic;
	FREQ : IN std_logic;
	REF_CLK : IN std_logic;
	FRQCOUNTBIN : OUT std_logic_vector(31 DOWNTO 0);
	REG_ACCES : IN std_logic
	);
END NDFC;

-- Design Ports Information


ARCHITECTURE structure OF NDFC IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_RST : std_logic;
SIGNAL ww_FREQ : std_logic;
SIGNAL ww_REF_CLK : std_logic;
SIGNAL ww_FRQCOUNTBIN : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_REG_ACCES : std_logic;
SIGNAL \FREQ~combout\ : std_logic;
SIGNAL \inst|DVCLK~regout\ : std_logic;
SIGNAL \REF_CLK~combout\ : std_logic;
SIGNAL \inst|PSCLK~regout\ : std_logic;
SIGNAL \inst|BSPCLK~regout\ : std_logic;
SIGNAL \REG_ACCES~combout\ : std_logic;
SIGNAL \inst1|COUNT[0]~3\ : std_logic;
SIGNAL \inst1|COUNT[1]~5\ : std_logic;
SIGNAL \inst1|COUNT[1]~5COUT1_65\ : std_logic;
SIGNAL \inst1|COUNT[2]~7\ : std_logic;
SIGNAL \inst1|COUNT[2]~7COUT1_66\ : std_logic;
SIGNAL \inst1|COUNT[3]~9\ : std_logic;
SIGNAL \inst1|COUNT[3]~9COUT1_67\ : std_logic;
SIGNAL \inst1|COUNT[4]~11\ : std_logic;
SIGNAL \inst1|COUNT[4]~11COUT1_68\ : std_logic;
SIGNAL \inst1|COUNT[5]~13\ : std_logic;
SIGNAL \inst1|COUNT[6]~15\ : std_logic;
SIGNAL \inst1|COUNT[6]~15COUT1_69\ : std_logic;
SIGNAL \inst1|COUNT[7]~17\ : std_logic;
SIGNAL \inst1|COUNT[7]~17COUT1_70\ : std_logic;
SIGNAL \inst1|COUNT[8]~19\ : std_logic;
SIGNAL \inst1|COUNT[8]~19COUT1_71\ : std_logic;
SIGNAL \inst1|COUNT[9]~21\ : std_logic;
SIGNAL \inst1|COUNT[9]~21COUT1_72\ : std_logic;
SIGNAL \inst1|COUNT[10]~23\ : std_logic;
SIGNAL \inst1|COUNT[11]~25\ : std_logic;
SIGNAL \inst1|COUNT[11]~25COUT1_73\ : std_logic;
SIGNAL \inst1|COUNT[12]~27\ : std_logic;
SIGNAL \inst1|COUNT[12]~27COUT1_74\ : std_logic;
SIGNAL \inst1|COUNT[13]~29\ : std_logic;
SIGNAL \inst1|COUNT[13]~29COUT1_75\ : std_logic;
SIGNAL \inst1|COUNT[14]~31\ : std_logic;
SIGNAL \inst1|COUNT[14]~31COUT1_76\ : std_logic;
SIGNAL \inst1|COUNT[15]~33\ : std_logic;
SIGNAL \inst1|COUNT[16]~35\ : std_logic;
SIGNAL \inst1|COUNT[16]~35COUT1_77\ : std_logic;
SIGNAL \inst1|COUNT[17]~37\ : std_logic;
SIGNAL \inst1|COUNT[17]~37COUT1_78\ : std_logic;
SIGNAL \inst1|COUNT[18]~39\ : std_logic;
SIGNAL \inst1|COUNT[18]~39COUT1_79\ : std_logic;
SIGNAL \inst1|COUNT[19]~41\ : std_logic;
SIGNAL \inst1|COUNT[19]~41COUT1_80\ : std_logic;
SIGNAL \inst1|COUNT[20]~43\ : std_logic;
SIGNAL \inst1|COUNT[21]~45\ : std_logic;
SIGNAL \inst1|COUNT[21]~45COUT1_81\ : std_logic;
SIGNAL \inst1|COUNT[22]~47\ : std_logic;
SIGNAL \inst1|COUNT[22]~47COUT1_82\ : std_logic;
SIGNAL \inst1|LessThan0~6_combout\ : std_logic;
SIGNAL \inst1|LessThan0~5_combout\ : std_logic;
SIGNAL \inst1|COUNT[23]~49\ : std_logic;
SIGNAL \inst1|COUNT[23]~49COUT1_83\ : std_logic;
SIGNAL \inst1|COUNT[24]~51\ : std_logic;
SIGNAL \inst1|COUNT[24]~51COUT1_84\ : std_logic;
SIGNAL \inst1|COUNT[25]~53\ : std_logic;
SIGNAL \inst1|COUNT[26]~55\ : std_logic;
SIGNAL \inst1|COUNT[26]~55COUT1_85\ : std_logic;
SIGNAL \inst1|COUNT[27]~57\ : std_logic;
SIGNAL \inst1|COUNT[27]~57COUT1_86\ : std_logic;
SIGNAL \inst1|COUNT[28]~59\ : std_logic;
SIGNAL \inst1|COUNT[28]~59COUT1_87\ : std_logic;
SIGNAL \inst1|COUNT[29]~61\ : std_logic;
SIGNAL \inst1|COUNT[29]~61COUT1_88\ : std_logic;
SIGNAL \inst1|LessThan0~8_combout\ : std_logic;
SIGNAL \inst1|LessThan0~7_combout\ : std_logic;
SIGNAL \inst1|LessThan0~9_combout\ : std_logic;
SIGNAL \inst1|LessThan0~3_combout\ : std_logic;
SIGNAL \inst1|LessThan0~2_combout\ : std_logic;
SIGNAL \inst1|LessThan0~1_combout\ : std_logic;
SIGNAL \inst1|LessThan0~0_combout\ : std_logic;
SIGNAL \inst1|LessThan0~4_combout\ : std_logic;
SIGNAL \inst1|process_0~0_combout\ : std_logic;
SIGNAL \inst1|COUNT[30]~63\ : std_logic;
SIGNAL \inst|COUNT\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst1|COUNT\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst1|FRQCNT\ : std_logic_vector(31 DOWNTO 0);

BEGIN

RST <= ww_RST;
ww_FREQ <= FREQ;
ww_REF_CLK <= REF_CLK;
FRQCOUNTBIN <= ww_FRQCOUNTBIN;
ww_REG_ACCES <= REG_ACCES;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: PIN_2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\FREQ~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_FREQ,
	combout => \FREQ~combout\);

-- Location: LC_X2_Y3_N9
\inst|COUNT[0]\ : maxii_lcell
-- Equation(s):
-- \inst|COUNT\(0) = DFFEAS((((!\inst|COUNT\(0)))), GLOBAL(\FREQ~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00ff",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst|COUNT\(0),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst|COUNT\(0));

-- Location: LC_X2_Y3_N8
\inst|DVCLK\ : maxii_lcell
-- Equation(s):
-- \inst|DVCLK~regout\ = DFFEAS((((\inst|COUNT\(0)))), GLOBAL(\FREQ~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst|COUNT\(0),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst|DVCLK~regout\);

-- Location: PIN_67,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\REF_CLK~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_REF_CLK,
	combout => \REF_CLK~combout\);

-- Location: LC_X7_Y1_N1
\inst|PSCLK\ : maxii_lcell
-- Equation(s):
-- \inst|PSCLK~regout\ = DFFEAS(GND, GLOBAL(\inst|DVCLK~regout\), VCC, , , \REF_CLK~combout\, , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst|DVCLK~regout\,
	datac => \REF_CLK~combout\,
	aclr => GND,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst|PSCLK~regout\);

-- Location: LC_X7_Y1_N9
\inst|BSPCLK\ : maxii_lcell
-- Equation(s):
-- \inst|BSPCLK~regout\ = DFFEAS(((\REF_CLK~combout\ & ((!\inst|PSCLK~regout\))) # (!\REF_CLK~combout\ & (\inst|BSPCLK~regout\ & \inst|PSCLK~regout\))), GLOBAL(\inst|DVCLK~regout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0cf0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \inst|DVCLK~regout\,
	datab => \inst|BSPCLK~regout\,
	datac => \REF_CLK~combout\,
	datad => \inst|PSCLK~regout\,
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst|BSPCLK~regout\);

-- Location: PIN_47,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\REG_ACCES~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_REG_ACCES,
	combout => \REG_ACCES~combout\);

-- Location: LC_X2_Y1_N4
\inst1|COUNT[0]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(0) = DFFEAS(\inst1|COUNT\(0) $ ((!\inst|BSPCLK~regout\)), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[0]~3\ = CARRY((\inst1|COUNT\(0) & (!\inst|BSPCLK~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9922",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(0),
	datab => \inst|BSPCLK~regout\,
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(0),
	cout => \inst1|COUNT[0]~3\);

-- Location: LC_X2_Y1_N5
\inst1|COUNT[1]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(1) = DFFEAS(\inst1|COUNT\(1) $ ((((\inst1|COUNT[0]~3\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[1]~5\ = CARRY(((!\inst1|COUNT[0]~3\)) # (!\inst1|COUNT\(1)))
-- \inst1|COUNT[1]~5COUT1_65\ = CARRY(((!\inst1|COUNT[0]~3\)) # (!\inst1|COUNT\(1)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(1),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[0]~3\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(1),
	cout0 => \inst1|COUNT[1]~5\,
	cout1 => \inst1|COUNT[1]~5COUT1_65\);

-- Location: LC_X2_Y1_N6
\inst1|COUNT[2]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(2) = DFFEAS((\inst1|COUNT\(2) $ ((!(!\inst1|COUNT[0]~3\ & \inst1|COUNT[1]~5\) # (\inst1|COUNT[0]~3\ & \inst1|COUNT[1]~5COUT1_65\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[2]~7\ = CARRY(((\inst1|COUNT\(2) & !\inst1|COUNT[1]~5\)))
-- \inst1|COUNT[2]~7COUT1_66\ = CARRY(((\inst1|COUNT\(2) & !\inst1|COUNT[1]~5COUT1_65\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(2),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[0]~3\,
	cin0 => \inst1|COUNT[1]~5\,
	cin1 => \inst1|COUNT[1]~5COUT1_65\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(2),
	cout0 => \inst1|COUNT[2]~7\,
	cout1 => \inst1|COUNT[2]~7COUT1_66\);

-- Location: LC_X2_Y1_N7
\inst1|COUNT[3]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(3) = DFFEAS(\inst1|COUNT\(3) $ (((((!\inst1|COUNT[0]~3\ & \inst1|COUNT[2]~7\) # (\inst1|COUNT[0]~3\ & \inst1|COUNT[2]~7COUT1_66\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[3]~9\ = CARRY(((!\inst1|COUNT[2]~7\)) # (!\inst1|COUNT\(3)))
-- \inst1|COUNT[3]~9COUT1_67\ = CARRY(((!\inst1|COUNT[2]~7COUT1_66\)) # (!\inst1|COUNT\(3)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(3),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[0]~3\,
	cin0 => \inst1|COUNT[2]~7\,
	cin1 => \inst1|COUNT[2]~7COUT1_66\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(3),
	cout0 => \inst1|COUNT[3]~9\,
	cout1 => \inst1|COUNT[3]~9COUT1_67\);

-- Location: LC_X2_Y1_N8
\inst1|COUNT[4]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(4) = DFFEAS(\inst1|COUNT\(4) $ ((((!(!\inst1|COUNT[0]~3\ & \inst1|COUNT[3]~9\) # (\inst1|COUNT[0]~3\ & \inst1|COUNT[3]~9COUT1_67\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[4]~11\ = CARRY((\inst1|COUNT\(4) & ((!\inst1|COUNT[3]~9\))))
-- \inst1|COUNT[4]~11COUT1_68\ = CARRY((\inst1|COUNT\(4) & ((!\inst1|COUNT[3]~9COUT1_67\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(4),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[0]~3\,
	cin0 => \inst1|COUNT[3]~9\,
	cin1 => \inst1|COUNT[3]~9COUT1_67\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(4),
	cout0 => \inst1|COUNT[4]~11\,
	cout1 => \inst1|COUNT[4]~11COUT1_68\);

-- Location: LC_X2_Y1_N9
\inst1|COUNT[5]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(5) = DFFEAS((\inst1|COUNT\(5) $ (((!\inst1|COUNT[0]~3\ & \inst1|COUNT[4]~11\) # (\inst1|COUNT[0]~3\ & \inst1|COUNT[4]~11COUT1_68\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[5]~13\ = CARRY(((!\inst1|COUNT[4]~11COUT1_68\) # (!\inst1|COUNT\(5))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(5),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[0]~3\,
	cin0 => \inst1|COUNT[4]~11\,
	cin1 => \inst1|COUNT[4]~11COUT1_68\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(5),
	cout => \inst1|COUNT[5]~13\);

-- Location: LC_X3_Y1_N0
\inst1|COUNT[6]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(6) = DFFEAS((\inst1|COUNT\(6) $ ((!\inst1|COUNT[5]~13\))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[6]~15\ = CARRY(((\inst1|COUNT\(6) & !\inst1|COUNT[5]~13\)))
-- \inst1|COUNT[6]~15COUT1_69\ = CARRY(((\inst1|COUNT\(6) & !\inst1|COUNT[5]~13\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(6),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[5]~13\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(6),
	cout0 => \inst1|COUNT[6]~15\,
	cout1 => \inst1|COUNT[6]~15COUT1_69\);

-- Location: LC_X3_Y1_N1
\inst1|COUNT[7]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(7) = DFFEAS((\inst1|COUNT\(7) $ (((!\inst1|COUNT[5]~13\ & \inst1|COUNT[6]~15\) # (\inst1|COUNT[5]~13\ & \inst1|COUNT[6]~15COUT1_69\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[7]~17\ = CARRY(((!\inst1|COUNT[6]~15\) # (!\inst1|COUNT\(7))))
-- \inst1|COUNT[7]~17COUT1_70\ = CARRY(((!\inst1|COUNT[6]~15COUT1_69\) # (!\inst1|COUNT\(7))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(7),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[5]~13\,
	cin0 => \inst1|COUNT[6]~15\,
	cin1 => \inst1|COUNT[6]~15COUT1_69\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(7),
	cout0 => \inst1|COUNT[7]~17\,
	cout1 => \inst1|COUNT[7]~17COUT1_70\);

-- Location: LC_X3_Y1_N2
\inst1|COUNT[8]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(8) = DFFEAS((\inst1|COUNT\(8) $ ((!(!\inst1|COUNT[5]~13\ & \inst1|COUNT[7]~17\) # (\inst1|COUNT[5]~13\ & \inst1|COUNT[7]~17COUT1_70\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[8]~19\ = CARRY(((\inst1|COUNT\(8) & !\inst1|COUNT[7]~17\)))
-- \inst1|COUNT[8]~19COUT1_71\ = CARRY(((\inst1|COUNT\(8) & !\inst1|COUNT[7]~17COUT1_70\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(8),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[5]~13\,
	cin0 => \inst1|COUNT[7]~17\,
	cin1 => \inst1|COUNT[7]~17COUT1_70\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(8),
	cout0 => \inst1|COUNT[8]~19\,
	cout1 => \inst1|COUNT[8]~19COUT1_71\);

-- Location: LC_X3_Y1_N3
\inst1|COUNT[9]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(9) = DFFEAS(\inst1|COUNT\(9) $ (((((!\inst1|COUNT[5]~13\ & \inst1|COUNT[8]~19\) # (\inst1|COUNT[5]~13\ & \inst1|COUNT[8]~19COUT1_71\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[9]~21\ = CARRY(((!\inst1|COUNT[8]~19\)) # (!\inst1|COUNT\(9)))
-- \inst1|COUNT[9]~21COUT1_72\ = CARRY(((!\inst1|COUNT[8]~19COUT1_71\)) # (!\inst1|COUNT\(9)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(9),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[5]~13\,
	cin0 => \inst1|COUNT[8]~19\,
	cin1 => \inst1|COUNT[8]~19COUT1_71\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(9),
	cout0 => \inst1|COUNT[9]~21\,
	cout1 => \inst1|COUNT[9]~21COUT1_72\);

-- Location: LC_X3_Y1_N4
\inst1|COUNT[10]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(10) = DFFEAS(\inst1|COUNT\(10) $ ((((!(!\inst1|COUNT[5]~13\ & \inst1|COUNT[9]~21\) # (\inst1|COUNT[5]~13\ & \inst1|COUNT[9]~21COUT1_72\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[10]~23\ = CARRY((\inst1|COUNT\(10) & ((!\inst1|COUNT[9]~21COUT1_72\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(10),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[5]~13\,
	cin0 => \inst1|COUNT[9]~21\,
	cin1 => \inst1|COUNT[9]~21COUT1_72\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(10),
	cout => \inst1|COUNT[10]~23\);

-- Location: LC_X3_Y1_N5
\inst1|COUNT[11]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(11) = DFFEAS(\inst1|COUNT\(11) $ ((((\inst1|COUNT[10]~23\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[11]~25\ = CARRY(((!\inst1|COUNT[10]~23\)) # (!\inst1|COUNT\(11)))
-- \inst1|COUNT[11]~25COUT1_73\ = CARRY(((!\inst1|COUNT[10]~23\)) # (!\inst1|COUNT\(11)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(11),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[10]~23\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(11),
	cout0 => \inst1|COUNT[11]~25\,
	cout1 => \inst1|COUNT[11]~25COUT1_73\);

-- Location: LC_X3_Y1_N6
\inst1|COUNT[12]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(12) = DFFEAS(\inst1|COUNT\(12) $ ((((!(!\inst1|COUNT[10]~23\ & \inst1|COUNT[11]~25\) # (\inst1|COUNT[10]~23\ & \inst1|COUNT[11]~25COUT1_73\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[12]~27\ = CARRY((\inst1|COUNT\(12) & ((!\inst1|COUNT[11]~25\))))
-- \inst1|COUNT[12]~27COUT1_74\ = CARRY((\inst1|COUNT\(12) & ((!\inst1|COUNT[11]~25COUT1_73\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(12),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[10]~23\,
	cin0 => \inst1|COUNT[11]~25\,
	cin1 => \inst1|COUNT[11]~25COUT1_73\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(12),
	cout0 => \inst1|COUNT[12]~27\,
	cout1 => \inst1|COUNT[12]~27COUT1_74\);

-- Location: LC_X3_Y1_N7
\inst1|COUNT[13]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(13) = DFFEAS((\inst1|COUNT\(13) $ (((!\inst1|COUNT[10]~23\ & \inst1|COUNT[12]~27\) # (\inst1|COUNT[10]~23\ & \inst1|COUNT[12]~27COUT1_74\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[13]~29\ = CARRY(((!\inst1|COUNT[12]~27\) # (!\inst1|COUNT\(13))))
-- \inst1|COUNT[13]~29COUT1_75\ = CARRY(((!\inst1|COUNT[12]~27COUT1_74\) # (!\inst1|COUNT\(13))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(13),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[10]~23\,
	cin0 => \inst1|COUNT[12]~27\,
	cin1 => \inst1|COUNT[12]~27COUT1_74\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(13),
	cout0 => \inst1|COUNT[13]~29\,
	cout1 => \inst1|COUNT[13]~29COUT1_75\);

-- Location: LC_X3_Y1_N8
\inst1|COUNT[14]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(14) = DFFEAS(\inst1|COUNT\(14) $ ((((!(!\inst1|COUNT[10]~23\ & \inst1|COUNT[13]~29\) # (\inst1|COUNT[10]~23\ & \inst1|COUNT[13]~29COUT1_75\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[14]~31\ = CARRY((\inst1|COUNT\(14) & ((!\inst1|COUNT[13]~29\))))
-- \inst1|COUNT[14]~31COUT1_76\ = CARRY((\inst1|COUNT\(14) & ((!\inst1|COUNT[13]~29COUT1_75\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(14),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[10]~23\,
	cin0 => \inst1|COUNT[13]~29\,
	cin1 => \inst1|COUNT[13]~29COUT1_75\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(14),
	cout0 => \inst1|COUNT[14]~31\,
	cout1 => \inst1|COUNT[14]~31COUT1_76\);

-- Location: LC_X3_Y1_N9
\inst1|COUNT[15]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(15) = DFFEAS((\inst1|COUNT\(15) $ (((!\inst1|COUNT[10]~23\ & \inst1|COUNT[14]~31\) # (\inst1|COUNT[10]~23\ & \inst1|COUNT[14]~31COUT1_76\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[15]~33\ = CARRY(((!\inst1|COUNT[14]~31COUT1_76\) # (!\inst1|COUNT\(15))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(15),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[10]~23\,
	cin0 => \inst1|COUNT[14]~31\,
	cin1 => \inst1|COUNT[14]~31COUT1_76\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(15),
	cout => \inst1|COUNT[15]~33\);

-- Location: LC_X4_Y1_N0
\inst1|COUNT[16]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(16) = DFFEAS((\inst1|COUNT\(16) $ ((!\inst1|COUNT[15]~33\))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[16]~35\ = CARRY(((\inst1|COUNT\(16) & !\inst1|COUNT[15]~33\)))
-- \inst1|COUNT[16]~35COUT1_77\ = CARRY(((\inst1|COUNT\(16) & !\inst1|COUNT[15]~33\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(16),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[15]~33\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(16),
	cout0 => \inst1|COUNT[16]~35\,
	cout1 => \inst1|COUNT[16]~35COUT1_77\);

-- Location: LC_X4_Y1_N1
\inst1|COUNT[17]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(17) = DFFEAS((\inst1|COUNT\(17) $ (((!\inst1|COUNT[15]~33\ & \inst1|COUNT[16]~35\) # (\inst1|COUNT[15]~33\ & \inst1|COUNT[16]~35COUT1_77\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[17]~37\ = CARRY(((!\inst1|COUNT[16]~35\) # (!\inst1|COUNT\(17))))
-- \inst1|COUNT[17]~37COUT1_78\ = CARRY(((!\inst1|COUNT[16]~35COUT1_77\) # (!\inst1|COUNT\(17))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(17),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[15]~33\,
	cin0 => \inst1|COUNT[16]~35\,
	cin1 => \inst1|COUNT[16]~35COUT1_77\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(17),
	cout0 => \inst1|COUNT[17]~37\,
	cout1 => \inst1|COUNT[17]~37COUT1_78\);

-- Location: LC_X4_Y1_N2
\inst1|COUNT[18]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(18) = DFFEAS((\inst1|COUNT\(18) $ ((!(!\inst1|COUNT[15]~33\ & \inst1|COUNT[17]~37\) # (\inst1|COUNT[15]~33\ & \inst1|COUNT[17]~37COUT1_78\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[18]~39\ = CARRY(((\inst1|COUNT\(18) & !\inst1|COUNT[17]~37\)))
-- \inst1|COUNT[18]~39COUT1_79\ = CARRY(((\inst1|COUNT\(18) & !\inst1|COUNT[17]~37COUT1_78\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(18),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[15]~33\,
	cin0 => \inst1|COUNT[17]~37\,
	cin1 => \inst1|COUNT[17]~37COUT1_78\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(18),
	cout0 => \inst1|COUNT[18]~39\,
	cout1 => \inst1|COUNT[18]~39COUT1_79\);

-- Location: LC_X4_Y1_N3
\inst1|COUNT[19]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(19) = DFFEAS(\inst1|COUNT\(19) $ (((((!\inst1|COUNT[15]~33\ & \inst1|COUNT[18]~39\) # (\inst1|COUNT[15]~33\ & \inst1|COUNT[18]~39COUT1_79\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[19]~41\ = CARRY(((!\inst1|COUNT[18]~39\)) # (!\inst1|COUNT\(19)))
-- \inst1|COUNT[19]~41COUT1_80\ = CARRY(((!\inst1|COUNT[18]~39COUT1_79\)) # (!\inst1|COUNT\(19)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(19),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[15]~33\,
	cin0 => \inst1|COUNT[18]~39\,
	cin1 => \inst1|COUNT[18]~39COUT1_79\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(19),
	cout0 => \inst1|COUNT[19]~41\,
	cout1 => \inst1|COUNT[19]~41COUT1_80\);

-- Location: LC_X4_Y1_N4
\inst1|COUNT[20]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(20) = DFFEAS(\inst1|COUNT\(20) $ ((((!(!\inst1|COUNT[15]~33\ & \inst1|COUNT[19]~41\) # (\inst1|COUNT[15]~33\ & \inst1|COUNT[19]~41COUT1_80\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[20]~43\ = CARRY((\inst1|COUNT\(20) & ((!\inst1|COUNT[19]~41COUT1_80\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(20),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[15]~33\,
	cin0 => \inst1|COUNT[19]~41\,
	cin1 => \inst1|COUNT[19]~41COUT1_80\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(20),
	cout => \inst1|COUNT[20]~43\);

-- Location: LC_X4_Y1_N5
\inst1|COUNT[21]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(21) = DFFEAS(\inst1|COUNT\(21) $ ((((\inst1|COUNT[20]~43\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[21]~45\ = CARRY(((!\inst1|COUNT[20]~43\)) # (!\inst1|COUNT\(21)))
-- \inst1|COUNT[21]~45COUT1_81\ = CARRY(((!\inst1|COUNT[20]~43\)) # (!\inst1|COUNT\(21)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(21),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[20]~43\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(21),
	cout0 => \inst1|COUNT[21]~45\,
	cout1 => \inst1|COUNT[21]~45COUT1_81\);

-- Location: LC_X4_Y1_N6
\inst1|COUNT[22]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(22) = DFFEAS(\inst1|COUNT\(22) $ ((((!(!\inst1|COUNT[20]~43\ & \inst1|COUNT[21]~45\) # (\inst1|COUNT[20]~43\ & \inst1|COUNT[21]~45COUT1_81\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[22]~47\ = CARRY((\inst1|COUNT\(22) & ((!\inst1|COUNT[21]~45\))))
-- \inst1|COUNT[22]~47COUT1_82\ = CARRY((\inst1|COUNT\(22) & ((!\inst1|COUNT[21]~45COUT1_81\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(22),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[20]~43\,
	cin0 => \inst1|COUNT[21]~45\,
	cin1 => \inst1|COUNT[21]~45COUT1_81\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(22),
	cout0 => \inst1|COUNT[22]~47\,
	cout1 => \inst1|COUNT[22]~47COUT1_82\);

-- Location: LC_X4_Y1_N7
\inst1|COUNT[23]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(23) = DFFEAS((\inst1|COUNT\(23) $ (((!\inst1|COUNT[20]~43\ & \inst1|COUNT[22]~47\) # (\inst1|COUNT[20]~43\ & \inst1|COUNT[22]~47COUT1_82\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[23]~49\ = CARRY(((!\inst1|COUNT[22]~47\) # (!\inst1|COUNT\(23))))
-- \inst1|COUNT[23]~49COUT1_83\ = CARRY(((!\inst1|COUNT[22]~47COUT1_82\) # (!\inst1|COUNT\(23))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(23),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[20]~43\,
	cin0 => \inst1|COUNT[22]~47\,
	cin1 => \inst1|COUNT[22]~47COUT1_82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(23),
	cout0 => \inst1|COUNT[23]~49\,
	cout1 => \inst1|COUNT[23]~49COUT1_83\);

-- Location: LC_X5_Y1_N8
\inst1|LessThan0~6\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~6_combout\ = (\inst1|COUNT\(21)) # ((\inst1|COUNT\(20)) # ((\inst1|COUNT\(22)) # (\inst1|COUNT\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(21),
	datab => \inst1|COUNT\(20),
	datac => \inst1|COUNT\(22),
	datad => \inst1|COUNT\(23),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~6_combout\);

-- Location: LC_X5_Y1_N7
\inst1|LessThan0~5\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~5_combout\ = (\inst1|COUNT\(17)) # ((\inst1|COUNT\(18)) # ((\inst1|COUNT\(19)) # (\inst1|COUNT\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(17),
	datab => \inst1|COUNT\(18),
	datac => \inst1|COUNT\(19),
	datad => \inst1|COUNT\(16),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~5_combout\);

-- Location: LC_X4_Y1_N8
\inst1|COUNT[24]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(24) = DFFEAS(\inst1|COUNT\(24) $ ((((!(!\inst1|COUNT[20]~43\ & \inst1|COUNT[23]~49\) # (\inst1|COUNT[20]~43\ & \inst1|COUNT[23]~49COUT1_83\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[24]~51\ = CARRY((\inst1|COUNT\(24) & ((!\inst1|COUNT[23]~49\))))
-- \inst1|COUNT[24]~51COUT1_84\ = CARRY((\inst1|COUNT\(24) & ((!\inst1|COUNT[23]~49COUT1_83\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(24),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[20]~43\,
	cin0 => \inst1|COUNT[23]~49\,
	cin1 => \inst1|COUNT[23]~49COUT1_83\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(24),
	cout0 => \inst1|COUNT[24]~51\,
	cout1 => \inst1|COUNT[24]~51COUT1_84\);

-- Location: LC_X4_Y1_N9
\inst1|COUNT[25]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(25) = DFFEAS((\inst1|COUNT\(25) $ (((!\inst1|COUNT[20]~43\ & \inst1|COUNT[24]~51\) # (\inst1|COUNT[20]~43\ & \inst1|COUNT[24]~51COUT1_84\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[25]~53\ = CARRY(((!\inst1|COUNT[24]~51COUT1_84\) # (!\inst1|COUNT\(25))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(25),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[20]~43\,
	cin0 => \inst1|COUNT[24]~51\,
	cin1 => \inst1|COUNT[24]~51COUT1_84\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(25),
	cout => \inst1|COUNT[25]~53\);

-- Location: LC_X5_Y1_N0
\inst1|COUNT[26]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(26) = DFFEAS((\inst1|COUNT\(26) $ ((!\inst1|COUNT[25]~53\))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[26]~55\ = CARRY(((\inst1|COUNT\(26) & !\inst1|COUNT[25]~53\)))
-- \inst1|COUNT[26]~55COUT1_85\ = CARRY(((\inst1|COUNT\(26) & !\inst1|COUNT[25]~53\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(26),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[25]~53\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(26),
	cout0 => \inst1|COUNT[26]~55\,
	cout1 => \inst1|COUNT[26]~55COUT1_85\);

-- Location: LC_X5_Y1_N1
\inst1|COUNT[27]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(27) = DFFEAS((\inst1|COUNT\(27) $ (((!\inst1|COUNT[25]~53\ & \inst1|COUNT[26]~55\) # (\inst1|COUNT[25]~53\ & \inst1|COUNT[26]~55COUT1_85\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[27]~57\ = CARRY(((!\inst1|COUNT[26]~55\) # (!\inst1|COUNT\(27))))
-- \inst1|COUNT[27]~57COUT1_86\ = CARRY(((!\inst1|COUNT[26]~55COUT1_85\) # (!\inst1|COUNT\(27))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(27),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[25]~53\,
	cin0 => \inst1|COUNT[26]~55\,
	cin1 => \inst1|COUNT[26]~55COUT1_85\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(27),
	cout0 => \inst1|COUNT[27]~57\,
	cout1 => \inst1|COUNT[27]~57COUT1_86\);

-- Location: LC_X5_Y1_N2
\inst1|COUNT[28]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(28) = DFFEAS((\inst1|COUNT\(28) $ ((!(!\inst1|COUNT[25]~53\ & \inst1|COUNT[27]~57\) # (\inst1|COUNT[25]~53\ & \inst1|COUNT[27]~57COUT1_86\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[28]~59\ = CARRY(((\inst1|COUNT\(28) & !\inst1|COUNT[27]~57\)))
-- \inst1|COUNT[28]~59COUT1_87\ = CARRY(((\inst1|COUNT\(28) & !\inst1|COUNT[27]~57COUT1_86\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datab => \inst1|COUNT\(28),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[25]~53\,
	cin0 => \inst1|COUNT[27]~57\,
	cin1 => \inst1|COUNT[27]~57COUT1_86\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(28),
	cout0 => \inst1|COUNT[28]~59\,
	cout1 => \inst1|COUNT[28]~59COUT1_87\);

-- Location: LC_X5_Y1_N3
\inst1|COUNT[29]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(29) = DFFEAS(\inst1|COUNT\(29) $ (((((!\inst1|COUNT[25]~53\ & \inst1|COUNT[28]~59\) # (\inst1|COUNT[25]~53\ & \inst1|COUNT[28]~59COUT1_87\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[29]~61\ = CARRY(((!\inst1|COUNT[28]~59\)) # (!\inst1|COUNT\(29)))
-- \inst1|COUNT[29]~61COUT1_88\ = CARRY(((!\inst1|COUNT[28]~59COUT1_87\)) # (!\inst1|COUNT\(29)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(29),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[25]~53\,
	cin0 => \inst1|COUNT[28]~59\,
	cin1 => \inst1|COUNT[28]~59COUT1_87\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(29),
	cout0 => \inst1|COUNT[29]~61\,
	cout1 => \inst1|COUNT[29]~61COUT1_88\);

-- Location: LC_X5_Y1_N4
\inst1|COUNT[30]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(30) = DFFEAS(\inst1|COUNT\(30) $ ((((!(!\inst1|COUNT[25]~53\ & \inst1|COUNT[29]~61\) # (\inst1|COUNT[25]~53\ & \inst1|COUNT[29]~61COUT1_88\))))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )
-- \inst1|COUNT[30]~63\ = CARRY((\inst1|COUNT\(30) & ((!\inst1|COUNT[29]~61COUT1_88\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(30),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[25]~53\,
	cin0 => \inst1|COUNT[29]~61\,
	cin1 => \inst1|COUNT[29]~61COUT1_88\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(30),
	cout => \inst1|COUNT[30]~63\);

-- Location: LC_X5_Y1_N6
\inst1|LessThan0~8\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~8_combout\ = (\inst1|COUNT\(31)) # ((\inst1|COUNT\(28)) # ((\inst1|COUNT\(30)) # (\inst1|COUNT\(29))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(31),
	datab => \inst1|COUNT\(28),
	datac => \inst1|COUNT\(30),
	datad => \inst1|COUNT\(29),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~8_combout\);

-- Location: LC_X5_Y1_N9
\inst1|LessThan0~7\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~7_combout\ = (\inst1|COUNT\(25)) # ((\inst1|COUNT\(27)) # ((\inst1|COUNT\(26)) # (\inst1|COUNT\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(25),
	datab => \inst1|COUNT\(27),
	datac => \inst1|COUNT\(26),
	datad => \inst1|COUNT\(24),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~7_combout\);

-- Location: LC_X6_Y1_N5
\inst1|LessThan0~9\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~9_combout\ = (\inst1|LessThan0~6_combout\) # ((\inst1|LessThan0~5_combout\) # ((\inst1|LessThan0~8_combout\) # (\inst1|LessThan0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~6_combout\,
	datab => \inst1|LessThan0~5_combout\,
	datac => \inst1|LessThan0~8_combout\,
	datad => \inst1|LessThan0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~9_combout\);

-- Location: LC_X6_Y1_N8
\inst1|LessThan0~3\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~3_combout\ = (\inst1|COUNT\(15)) # ((\inst1|COUNT\(13)) # ((\inst1|COUNT\(14)) # (\inst1|COUNT\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(15),
	datab => \inst1|COUNT\(13),
	datac => \inst1|COUNT\(14),
	datad => \inst1|COUNT\(12),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~3_combout\);

-- Location: LC_X6_Y1_N9
\inst1|LessThan0~2\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~2_combout\ = (\inst1|COUNT\(11)) # ((\inst1|COUNT\(9)) # ((\inst1|COUNT\(8)) # (\inst1|COUNT\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(11),
	datab => \inst1|COUNT\(9),
	datac => \inst1|COUNT\(8),
	datad => \inst1|COUNT\(10),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~2_combout\);

-- Location: LC_X6_Y1_N4
\inst1|LessThan0~1\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~1_combout\ = (\inst1|COUNT\(4)) # ((\inst1|COUNT\(5)) # ((\inst1|COUNT\(7)) # (\inst1|COUNT\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(4),
	datab => \inst1|COUNT\(5),
	datac => \inst1|COUNT\(7),
	datad => \inst1|COUNT\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~1_combout\);

-- Location: LC_X2_Y1_N3
\inst1|LessThan0~0\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~0_combout\ = (\inst1|COUNT\(2)) # ((\inst1|COUNT\(3)) # ((\inst1|COUNT\(1)) # (\inst1|COUNT\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|COUNT\(2),
	datab => \inst1|COUNT\(3),
	datac => \inst1|COUNT\(1),
	datad => \inst1|COUNT\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~0_combout\);

-- Location: LC_X6_Y1_N6
\inst1|LessThan0~4\ : maxii_lcell
-- Equation(s):
-- \inst1|LessThan0~4_combout\ = (\inst1|LessThan0~3_combout\) # ((\inst1|LessThan0~2_combout\) # ((\inst1|LessThan0~1_combout\) # (\inst1|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~3_combout\,
	datab => \inst1|LessThan0~2_combout\,
	datac => \inst1|LessThan0~1_combout\,
	datad => \inst1|LessThan0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|LessThan0~4_combout\);

-- Location: LC_X6_Y1_N7
\inst1|process_0~0\ : maxii_lcell
-- Equation(s):
-- \inst1|process_0~0_combout\ = (!\REG_ACCES~combout\ & (\inst|BSPCLK~regout\ & ((\inst1|LessThan0~9_combout\) # (\inst1|LessThan0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4440",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REG_ACCES~combout\,
	datab => \inst|BSPCLK~regout\,
	datac => \inst1|LessThan0~9_combout\,
	datad => \inst1|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|process_0~0_combout\);

-- Location: LC_X5_Y1_N5
\inst1|COUNT[31]\ : maxii_lcell
-- Equation(s):
-- \inst1|COUNT\(31) = DFFEAS(\inst1|COUNT\(31) $ ((((\inst1|COUNT[30]~63\)))), GLOBAL(\FREQ~combout\), VCC, , , , , \inst1|process_0~0_combout\, )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5a",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	dataa => \inst1|COUNT\(31),
	aclr => GND,
	sclr => \inst1|process_0~0_combout\,
	cin => \inst1|COUNT[30]~63\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|COUNT\(31));

-- Location: LC_X7_Y1_N0
\inst1|FRQCNT[31]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(31) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(31), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(31),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(31));

-- Location: LC_X6_Y1_N3
\inst1|FRQCNT[30]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(30) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(30), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(30),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(30));

-- Location: LC_X6_Y1_N2
\inst1|FRQCNT[29]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(29) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(29), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(29),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(29));

-- Location: LC_X7_Y1_N2
\inst1|FRQCNT[28]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(28) = DFFEAS((((\inst1|COUNT\(28)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(28),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(28));

-- Location: LC_X5_Y2_N6
\inst1|FRQCNT[27]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(27) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(27), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(27),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(27));

-- Location: LC_X6_Y2_N5
\inst1|FRQCNT[26]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(26) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(26), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(26),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(26));

-- Location: LC_X5_Y2_N0
\inst1|FRQCNT[25]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(25) = DFFEAS((((\inst1|COUNT\(25)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(25),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(25));

-- Location: LC_X5_Y2_N5
\inst1|FRQCNT[24]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(24) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(24), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(24),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(24));

-- Location: LC_X5_Y2_N4
\inst1|FRQCNT[23]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(23) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(23), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(23),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(23));

-- Location: LC_X5_Y2_N3
\inst1|FRQCNT[22]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(22) = DFFEAS((((\inst1|COUNT\(22)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(22),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(22));

-- Location: LC_X5_Y2_N8
\inst1|FRQCNT[21]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(21) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(21), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(21),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(21));

-- Location: LC_X5_Y2_N2
\inst1|FRQCNT[20]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(20) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(20), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(20),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(20));

-- Location: LC_X5_Y2_N9
\inst1|FRQCNT[19]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(19) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(19), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(19),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(19));

-- Location: LC_X7_Y1_N3
\inst1|FRQCNT[18]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(18) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(18), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(18),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(18));

-- Location: LC_X5_Y2_N7
\inst1|FRQCNT[17]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(17) = DFFEAS((((\inst1|COUNT\(17)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(17),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(17));

-- Location: LC_X6_Y1_N1
\inst1|FRQCNT[16]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(16) = DFFEAS((((\inst1|COUNT\(16)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(16),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(16));

-- Location: LC_X7_Y1_N5
\inst1|FRQCNT[15]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(15) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(15), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(15),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(15));

-- Location: LC_X7_Y1_N6
\inst1|FRQCNT[14]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(14) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(14), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(14),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(14));

-- Location: LC_X7_Y1_N4
\inst1|FRQCNT[13]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(13) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(13), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(13),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(13));

-- Location: LC_X7_Y1_N7
\inst1|FRQCNT[12]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(12) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(12), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(12),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(12));

-- Location: LC_X6_Y1_N0
\inst1|FRQCNT[11]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(11) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(11), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(11),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(11));

-- Location: LC_X5_Y2_N1
\inst1|FRQCNT[10]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(10) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(10), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(10),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(10));

-- Location: LC_X6_Y2_N4
\inst1|FRQCNT[9]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(9) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(9), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(9),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(9));

-- Location: LC_X6_Y2_N6
\inst1|FRQCNT[8]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(8) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(8), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(8),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(8));

-- Location: LC_X7_Y1_N8
\inst1|FRQCNT[7]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(7) = DFFEAS((((\inst1|COUNT\(7)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(7),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(7));

-- Location: LC_X6_Y2_N1
\inst1|FRQCNT[6]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(6) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(6), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(6),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(6));

-- Location: LC_X2_Y1_N0
\inst1|FRQCNT[5]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(5) = DFFEAS((((\inst1|COUNT\(5)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(5),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(5));

-- Location: LC_X6_Y2_N8
\inst1|FRQCNT[4]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(4) = DFFEAS((((\inst1|COUNT\(4)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(4),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(4));

-- Location: LC_X5_Y3_N4
\inst1|FRQCNT[3]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(3) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(3), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(3),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(3));

-- Location: LC_X2_Y1_N1
\inst1|FRQCNT[2]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(2) = DFFEAS((((\inst1|COUNT\(2)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datad => \inst1|COUNT\(2),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(2));

-- Location: LC_X2_Y1_N2
\inst1|FRQCNT[1]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(1) = DFFEAS((((\inst1|COUNT\(1)))), GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(1),
	aclr => GND,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(1));

-- Location: LC_X5_Y3_N2
\inst1|FRQCNT[0]\ : maxii_lcell
-- Equation(s):
-- \inst1|FRQCNT\(0) = DFFEAS(GND, GLOBAL(\FREQ~combout\), VCC, , \inst1|process_0~0_combout\, \inst1|COUNT\(0), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \FREQ~combout\,
	datac => \inst1|COUNT\(0),
	aclr => GND,
	sload => VCC,
	ena => \inst1|process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|FRQCNT\(0));

-- Location: PIN_29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\RST~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst|BSPCLK~regout\,
	oe => VCC,
	padio => ww_RST);

-- Location: PIN_53,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[31]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(31),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(31));

-- Location: PIN_44,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[30]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(30),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(30));

-- Location: PIN_37,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[29]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(29),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(29));

-- Location: PIN_56,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[28]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(28),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(28));

-- Location: PIN_39,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[27]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(27),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(27));

-- Location: PIN_66,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[26]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(26),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(26));

-- Location: PIN_41,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[25]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(25),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(25));

-- Location: PIN_36,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[24]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(24),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(24));

-- Location: PIN_86,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[23]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(23),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(23));

-- Location: PIN_17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[22]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(22),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(22));

-- Location: PIN_87,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[21]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(21),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(21));

-- Location: PIN_42,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[20]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(20),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(20));

-- Location: PIN_38,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[19]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(19),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(19));

-- Location: PIN_55,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[18]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(18),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(18));

-- Location: PIN_40,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[17]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(17),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(17));

-- Location: PIN_48,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[16]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(16),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(16));

-- Location: PIN_51,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[15]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(15),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(15));

-- Location: PIN_50,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[14]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(14),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(14));

-- Location: PIN_54,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[13]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(13),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(13));

-- Location: PIN_49,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[12]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(12),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(12));

-- Location: PIN_43,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[11]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(11),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(11));

-- Location: PIN_62,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[10]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(10),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(10));

-- Location: PIN_68,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[9]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(9),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(9));

-- Location: PIN_57,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[8]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(8),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(8));

-- Location: PIN_52,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(7),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(7));

-- Location: PIN_61,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(6),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(6));

-- Location: PIN_19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(5),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(5));

-- Location: PIN_58,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(4),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(4));

-- Location: PIN_85,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(3),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(3));

-- Location: PIN_21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(2),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(2));

-- Location: PIN_20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(1),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(1));

-- Location: PIN_88,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\FRQCOUNTBIN[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|FRQCNT\(0),
	oe => VCC,
	padio => ww_FRQCOUNTBIN(0));
END structure;


