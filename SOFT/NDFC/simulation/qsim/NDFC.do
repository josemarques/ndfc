onerror {exit -code 1}
vlib work
vcom -work work NDFC.vho
vcom -work work Waveform.vwf.vht
vsim -c -t 1ps -sdfmax NDFC_vhd_vec_tst/i1=NDFC_vhd.sdo -L maxii -L altera -L altera_mf -L 220model -L sgate -L altera_lnsim work.NDFC_vhd_vec_tst
vcd file -direction NDFC.msim.vcd
vcd add -internal NDFC_vhd_vec_tst/*
vcd add -internal NDFC_vhd_vec_tst/i1/*
proc simTimestamp {} {
    echo "Simulation time: $::now ps"
    if { [string equal running [runStatus]] } {
        after 2500 simTimestamp
    }
}
after 2500 simTimestamp
run -all
quit -f


