-- Copyright (C) 2022  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "07/25/2022 12:19:20"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          NCOUNT
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY NCOUNT_vhd_vec_tst IS
END NCOUNT_vhd_vec_tst;
ARCHITECTURE NCOUNT_arch OF NCOUNT_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL Clk : STD_LOGIC;
SIGNAL En : STD_LOGIC;
SIGNAL output : STD_LOGIC_VECTOR(5 DOWNTO 0);
SIGNAL Rst : STD_LOGIC;
COMPONENT NCOUNT
	PORT (
	Clk : IN STD_LOGIC;
	En : IN STD_LOGIC;
	output : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
	Rst : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : NCOUNT
	PORT MAP (
-- list connections between master ports and signals
	Clk => Clk,
	En => En,
	output => output,
	Rst => Rst
	);

-- Clk
t_prcs_Clk: PROCESS
BEGIN
LOOP
	Clk <= '0';
	WAIT FOR 2500 ps;
	Clk <= '1';
	WAIT FOR 2500 ps;
	IF (NOW >= 200000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_Clk;

-- En
t_prcs_En: PROCESS
BEGIN
LOOP
	En <= '0';
	WAIT FOR 50000 ps;
	En <= '1';
	WAIT FOR 50000 ps;
	IF (NOW >= 200000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_En;

-- Rst
t_prcs_Rst: PROCESS
BEGIN
LOOP
	Rst <= '0';
	WAIT FOR 99000 ps;
	Rst <= '1';
	WAIT FOR 1000 ps;
	IF (NOW >= 200000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_Rst;
END NCOUNT_arch;
