--Dated 05/August/2019
--https://www.engineersgarage.com/n-bit-gray-counter-using-vhdl/
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY NGPRECNT IS --N bit gray pre counter
  GENERIC (N: integer := 32);
  PORT (FRQSIG, RESET, ENAB: IN std_logic;
  		DIV: OUT std_logic;
        FRQCNT: OUT std_logic_vector (N-1 DOWNTO 0));
END NGPRECNT;

ARCHITECTURE GRAYPRECNT OF NGPRECNT IS
  SIGNAL CURRSTAT, NXSTAT, HLD, NXTHLD: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
BEGIN

  StateReg: PROCESS (FRQSIG)
  BEGIN
    IF (FRQSIG = '1' AND FRQSIG'EVENT) THEN
      IF (RESET = '1') THEN
        CURRSTAT <= (OTHERS =>'0');
      ELSIF (ENAB = '1') THEN
        CURRSTAT <= NXSTAT;
      END IF;
    END IF;
  END PROCESS;


  HLD <= CURRSTAT XOR ('0' & HLD(N-1 DOWNTO 1));
  NXTHLD <= std_logic_vector(unsigned(HLD) + 1);
  NXSTAT <= NXTHLD XOR ('0' & NXTHLD(N-1 DOWNTO 1)); 
  FRQCNT <= CURRSTAT;

   DIV<='1' when CURRSTAT=std_logic_vector(to_unsigned(0,CURRSTAT'length)) else '0';
  
END GRAYPRECNT;