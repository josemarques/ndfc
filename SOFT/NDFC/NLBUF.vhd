--N bit Latch Buffer--

Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;
entity NLBFF is
	generic(N: integer :=32);--Number of bits of the counter
	port(	LATCH : IN bit;--Latch signal
			IDATA : IN std_logic_vector (N-1 DOWNTO 0);--Input data
			ODATA : OUT std_logic_vector (N-1 DOWNTO 0)--Latched data
	    );
end NLBFF;
--
architecture N_LATCH_BUFFER_ARCH of NLBFF is
	signal DATA: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
	
begin

	process(LATCH)	
begin
	if(LATCH'event and LATCH='1')then
		DATA<=IDATA;
		end if;
	end process;
	ODATA<=DATA;

	--DATA<=IDATA WHEN LATCH='1' ELSE DATA;
	--ODATA<=DATA;
	
end N_LATCH_BUFFER_ARCH;
--BUFFER_REG_N--